/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.etsii.ull.controlasistencia.Business;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.gson.Gson;
import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import es.etsii.ull.controlasistencia.Data.BaseDatos;
import es.etsii.ull.controlasistencia.R;

/**
 * An activity to illustrate how to pick a file with the
 * opener intent.
 */
public class PickFileWithOpenerActivity extends BaseDriveActivity {

    private static final String TAG = "PickFileWithOpenerActivity";

    private static final int REQUEST_CODE_OPENER = 1;

    private DriveId driveId;

    private ProgressDialog pDialog;

    private static String PAQUETE = "es.etsii.ull.controlasistencia";

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
                .setMimeType(new String[] { "text/csv" })
                .build(getGoogleApiClient());
        try {
            startIntentSenderForResult(
                    intentSender, REQUEST_CODE_OPENER, null, 0, 0, 0);
        } catch (SendIntentException e) {
          Log.d(">>", "Unable to send intent");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_CODE_OPENER:
            if (resultCode == RESULT_OK) {
                if (data == null) {

                } else {
                    final Context contexto = this;
                    pDialog = new ProgressDialog(contexto);
                    pDialog.setMessage("Descargando base de datos...");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    driveId = (DriveId) data.getParcelableExtra(
                            OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);
                    // showMessage("Selected file's ID: " + driveId);



                    ResultCallback<DriveApi.DriveContentsResult> contentsOpenedCallback =
                            new ResultCallback<DriveApi.DriveContentsResult>() {
                                @Override

                                public void onResult(DriveApi.DriveContentsResult result) {
                                    if (!result.getStatus().isSuccess()) {
                                        // display an error saying file can't be opened
                                        Log.d("Error:","No se puede abrir el archivo o no se encuentra");
                                        return;
                                    }

                                    // DriveContents object contains pointers
                                    // to the actual byte stream
                                    DriveContents contents = result.getDriveContents();

                                    BufferedReader reader = new BufferedReader(new InputStreamReader(contents.getInputStream()));
                                    StringBuilder builder = new StringBuilder();
                                    String line;
                                    List<String[]> data = new ArrayList<String[]>();
                                    try {

                                        while ((line = reader.readLine()) != null) {
                                            line = line.replaceAll("\"", "");
                                            Log.d(">>>> Line = " , line);
                                            data.add(line.split(","));
                                            builder.append(line);
                                            builder.append("\n");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    String contentsAsString = builder.toString();

                                    try {

                                        CSVWriter writer = new CSVWriter(new FileWriter("/data/data/es.etsii.ull.controlasistencia/data/dbDownloaded.csv"));

                                        writer.writeAll(data);

                                        writer.close();

                                        BaseDatos bd = new BaseDatos();
                                        try {
                                            bd = new BaseDatos();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        AlertDialog.Builder alert = new AlertDialog.Builder(contexto);

                                        alert.setTitle("Nombre de la asignatura");
                                        alert.setMessage("Por favor, introduzca un nombre de la asignatura.");

                                        // Set an EditText view to get user input
                                        final EditText input = new EditText(contexto);
                                        alert.setView(input);

                                        final BaseDatos finalBd = bd;
                                        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                SharedPreferences prefs = contexto.getSharedPreferences(
                                                        PAQUETE, Context.MODE_PRIVATE);
                                                String asignaturaActual = PAQUETE + ".asignaturaActual";
                                                String asignaturasAlmacenadas = PAQUETE + ".asignaturasAlmacenadas";
                                                List<String> arrayAsginaturas;
                                                if (!prefs.contains(asignaturasAlmacenadas)) {
                                                    arrayAsginaturas = new ArrayList<String>();
                                                    prefs.edit().putString(asignaturasAlmacenadas, new Gson().toJson(arrayAsginaturas)).apply();
                                                }


                                                InputMethodManager imm = (InputMethodManager)getSystemService(
                                                        Context.INPUT_METHOD_SERVICE);
                                                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                                                Editable value = input.getText();

                                                arrayAsginaturas = new Gson().fromJson(prefs.getString(asignaturasAlmacenadas, ""), ArrayList.class);
                                                arrayAsginaturas.add(value.toString().toUpperCase());
                                                prefs.edit().putString(asignaturasAlmacenadas, new Gson().toJson(arrayAsginaturas)).apply();
                                                prefs.edit().putString(asignaturaActual, value.toString().toUpperCase()).apply();

                                                finalBd.connect();
                                                //  bd.crearTabla();
                                                finalBd.importCsv(value.toString());
                                                finalBd.disconnect();
                                                finalBd.connect();
                                                finalBd.exportCsv(contexto);
                                                finalBd.disconnect();

                                                Log.d(">>>>", value.toString().toUpperCase());

                                                finish();
                                            }
                                        });

                                        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        });

                                        alert.show();



                                    if (pDialog.isShowing())
                                            pDialog.dismiss();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("RESULT:",contentsAsString);



                                    getGoogleApiClient().disconnect();

                                }
                            };



                    DriveFile file = Drive.DriveApi.getFile(getGoogleApiClient(), driveId);
                    // showMessage("Selected file's ID: " + file.getDriveId().encodeToString());
                    file.open(getGoogleApiClient(), DriveFile.MODE_READ_ONLY, null)
                            .setResultCallback(contentsOpenedCallback);
                }

            }

            break;
        default:
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
