package es.etsii.ull.controlasistencia.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import es.ava.aruco.CameraParameters;
import es.ava.aruco.Marker;
import es.ava.aruco.MarkerDetector;
import es.etsii.ull.controlasistencia.Data.BaseDatos;
import es.etsii.ull.controlasistencia.R;

/**
 * Clase que gestiona la ventana que captura los marcadores
 */
public class CameraDetection extends Activity implements CvCameraViewListener2 {
    private static final String TAG                 = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;

    private Mat                  mIntermediateMat;

    private BaseDatos bd;
    private static String niuDetectado;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public CameraDetection() {}

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.camera_detection);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.image_manipulations_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        try {
            this.bd = new BaseDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Log.i(TAG, "called onCreateOptionsMenu");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        return true;
    }

    public void onCameraViewStarted(int width, int height) {

    }

    public void onCameraViewStopped() {
        // Explicitly deallocate Mats
        if (mIntermediateMat != null)
            mIntermediateMat.release();

        mIntermediateMat = null;
    }

    /**
     * Método que es llamado cada vez que se captura un frame de la cámara
     * @param inputFrame
     * @return
     */
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        Mat rgba = inputFrame.rgba();
        Size sizeRgba = rgba.size();

        MarkerDetector mkd = new MarkerDetector();
        Vector<Marker> markers = new Vector<Marker>();

        CameraParameters mCamParam = new CameraParameters();
        mCamParam.readFromXML(Environment.getExternalStorageDirectory().getAbsolutePath() + "/calibration/camera.xml");

        Mat debug = new Mat();

        mkd.detect(rgba, markers, mCamParam, (float) 0.05, debug); // Llamada a ArUco para identificar los marcadores en la imagen rgba

        // Si se detectan los cuatro submarcadores del marcador
        if (markers.size() == 4) {
            String binary = "";
            // Se obtiene el numero binario codificado con las rotaciones
            for (int i = 0; i < markers.size(); i++) {
                if (markers.get(i).getRotations() == 0)
                    binary += "00";
                else if (markers.get(i).getRotations() == 1)
                    binary += "01";
                else if (markers.get(i).getRotations() == 2)
                    binary += "10";
                else if (markers.get(i).getRotations() == 3)
                    binary += "11";
            }
            String niu = "";
            int valor = Integer.parseInt(binary, 2);
            if (valor < 10) {
                niu += "0" + Integer.parseInt(binary, 2);
            } else {
                niu += Integer.parseInt(binary, 2);
            }


            // Se obtiene de cada identificador de marcador el valor que codifica
            int resta = 100;
            for (int i = 0; i < markers.size(); i++) {

                if (markers.get(i).getMarkerId() - resta < 10)
                    niu += "0" + String.valueOf(markers.get(i).getMarkerId() - resta);
                else
                    niu += String.valueOf(markers.get(i).getMarkerId() - resta);

                resta += 100;
                markers.get(i).draw(rgba, new Scalar(11, 243, 50), 2, false);
            }
            bd.connect();
            if ((bd.obtenerAlumno(niu, getApplicationContext()))) {
                niuDetectado = niu;
                String alumno = bd.getNombre() + " " + bd.getApellido1() + " " + bd.getApellido2();
                Log.d("alumno", niu);
                // Se añade la información del alumno como Extra del intent que muestra dicha información
                Intent i = new Intent(this, AlumnoDetectado.class);
                i.putExtra("niu", niu);
                i.putExtra("nombre", bd.getNombre());
                i.putExtra("apellido1", bd.getApellido1());
                i.putExtra("apellido2", bd.getApellido2());
                i.putExtra("foto", bd.getFoto());
                bd.disconnect();
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            } else {
                System.out.println("El alumno " + niu + " ya ha sido registrado o no existe en la base de datos.");
            }
            bd.disconnect();
        } else if (markers.size() == 1) {
            markers.get(0).draw(rgba, new Scalar(255,0,0), 2, false);
        } else if (markers.size() == 2) {
            for (int i = 0; i < markers.size(); i++) {
                markers.get(i).draw(rgba, new Scalar(255,128,0), 2, false);
            }
        } else if (markers.size() == 3) {
            for (int i = 0; i < markers.size(); i++) {
                markers.get(i).draw(rgba, new Scalar(255,255,0), 2, false);
            }
        }

        return rgba;
    }

    // Cuando presione la tecla atrás le pregunto si quiere salir
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in,R.anim.right_out);
        }

        return false;
    }
}
