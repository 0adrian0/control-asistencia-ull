package es.etsii.ull.controlasistencia.Data;
/**
 * Clase que gestiona la base de datos H2 con los alumnos de las asignaturas
 */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.AccessibleObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDatos extends Activity {
	private static String Nombre;
	private static String Apellido1;
	private static String Apellido2;
    private static String Niu;
    private static String Foto;
	private static Connection conn;
    private static String PAQUETE = "es.etsii.ull.controlasistencia";
	
    public BaseDatos() throws Exception {
    	Class.forName("org.h2.Driver");
	}
    
    public void connect() {
    	 try {

             String url = "jdbc:h2:/data/data/" +
                     "es.etsii.ull.controlasistencia" +
                     "/data/database" +
                     ";FILE_LOCK=FS" +
                     ";PAGE_SIZE=1024" +
                     ";CACHE_SIZE=8192";

             conn = DriverManager.getConnection(url);

			//conn = DriverManager.getConnection("jdbc:h2:/data/data/", "sa", "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Método que permite exportar las listas de la asignatura actual seleccionada a un fichero CSV
     * @param ctx
     */
    public void exportCsv(Context ctx) {
        Statement stat;
        SharedPreferences prefs = ctx.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        String nombreAsignatura = PAQUETE + ".asignaturaActual";

        String asignatura = prefs.getString(nombreAsignatura,"");

        try {
            stat = conn.createStatement();
            stat.execute("CALL CSVWRITE('/data/data/es.etsii.ull.controlasistencia/data/AsistenciaAlumnos.csv', 'SELECT * FROM \"" + asignatura.toUpperCase() + "\"');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que borra de la base de datos la asignatura fijada como asignatura actual
     * @param ctx
     */
    public void borrarAsignatura(Context ctx) {
        Statement stat;
        SharedPreferences prefs = ctx.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        String nombreAsignatura = PAQUETE + ".asignaturaActual";

        String asignatura = prefs.getString(nombreAsignatura,"");

        try {
            stat = conn.createStatement();
            stat.execute("DROP TABLE IF EXISTS \"" + asignatura.toUpperCase() + "\"" );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que carga en la base de datos una asignatura desde un fichero CSV. El nombre de la misma se pasa por parámetro.
     * @param asignatura
     */
    public void importCsv(String asignatura) {
        Statement stat;
        try {
            stat = conn.createStatement();
            stat.execute("DROP TABLE IF EXISTS \"" + asignatura.toUpperCase() + "\"");
            stat.execute("CREATE TABLE \"" + asignatura.toUpperCase() + "\" AS SELECT * FROM CSVREAD('/data/data/es.etsii.ull.controlasistencia/data/dbDownloaded.csv');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que cierra la conexión abierta con la base de datos
     */
    public void disconnect() {
    	try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Método que obtiene la información de un alumno de la asignatura actual con el niu que se pasa
     * por parámetro.
     * @param niu
     * @param ctx
     * @return
     */
    public boolean obtenerAlumno(String niu, Context ctx)  {
    	Statement stat;
    	ResultSet rs;
        SharedPreferences prefs = ctx.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        String nombreAsignatura = PAQUETE + ".asignaturaActual";

        String asignatura = prefs.getString(nombreAsignatura,"");
	    try {
			stat = conn.createStatement();
			rs = stat.executeQuery("select * from \"" + asignatura.toUpperCase() + "\" WHERE NIU = '" + niu + "'");

			if (rs.next()) {
                Niu = niu;
				Nombre = rs.getString("NOMBRE");
				Apellido1 = rs.getString("APELLIDO1");
				Apellido2 = rs.getString("APELLIDO2");
                Foto = rs.getString("FOTO");
                return true;
			} else {
				return false;
			}
				
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false; 
	    
    }

    /**
     * Método que añade una columna a la tabla de la asignatura actual con la fecha del día. Esta nueva
     * columa se utiliza para apuntar la asistencia de los alumnos
     * @param fecha
     * @param ctx
     */
    public void introducirFecha(String fecha, Context ctx) {
        Statement stat;
        SharedPreferences prefs = ctx.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        String nombreAsignatura = PAQUETE + ".asignaturaActual";

        String asignatura = prefs.getString(nombreAsignatura,"");
        try {
            stat = conn.createStatement();
            stat.execute(" ALTER TABLE \"" + asignatura.toUpperCase() + "\" ADD COLUMN \"" + fecha + "\" VARCHAR(100);");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que indica que un alumno asiste en una fecha concreta
     * @param niu
     * @param fecha
     * @param ctx
     */
    public void alumnoAsiste(String niu, String fecha, Context ctx) {
        Statement stat;
        SharedPreferences prefs = ctx.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        String nombreAsignatura = PAQUETE + ".asignaturaActual";

        String asignatura = prefs.getString(nombreAsignatura,"");
        try {
            stat = conn.createStatement();
            stat.execute(" UPDATE \"" + asignatura.toUpperCase() + "\" SET \"" + fecha + "\" = 'Asiste' WHERE NIU = '" + niu + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido1() {
		return Apellido1;
	}
	public void setApellido1(String apellido1) {
		Apellido1 = apellido1;
	}
	public String getApellido2() {
		return Apellido2;
	}
	public void setApellido2(String apellido2) {
		Apellido2 = apellido2;
	}
    public static String getNiu() {
        return Niu;
    }
    public static void setNiu(String niu) {
        Niu = niu;
    }
    public static String getFoto() {
        return Foto;
    }
    public static void setFoto(String foto) {
        Foto = foto;
    }
}