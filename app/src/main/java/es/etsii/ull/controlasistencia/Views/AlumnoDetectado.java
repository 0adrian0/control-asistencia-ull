package es.etsii.ull.controlasistencia.Views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Calendar;
import es.etsii.ull.controlasistencia.Data.BaseDatos;
import es.etsii.ull.controlasistencia.R;

/**
 * Clase que gestiona la ventana que muestra la información de un alumno cuando es reconocido.
 */
public class AlumnoDetectado extends ActionBarActivity {
    private BaseDatos bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_detectado);

        try {
            this.bd = new BaseDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // La información del alumno se extrae del Intent
        Intent intent = getIntent();
        final String niu = intent.getStringExtra("niu");
        String nombre = intent.getStringExtra("nombre");
        String apellido1 = intent.getStringExtra("apellido1");
        String apellido2 = intent.getStringExtra("apellido2");
        String foto = intent.getStringExtra("foto");

        TextView t = (TextView) findViewById(R.id.textViewNombre);
        t.setText("Nombre: " + nombre);
        t = (TextView) findViewById(R.id.textViewApellidos);
        t.setText("Apellidos: " + apellido1 + " " + apellido2);
        t = (TextView) findViewById(R.id.textViewNiu);
        t.setText("NIU: " + niu);

        // Se convierte la foto de String a Bitmap
        byte[] decodedString = Base64.decode(foto, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ImageView img = (ImageView) findViewById(R.id.imageViewFoto);
        img.setImageBitmap(decodedByte);

        Button bt = (Button) findViewById(R.id.buttonConfirmar);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                String dia = Integer.toString(c.get(Calendar.DATE));
                String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
                String annio = Integer.toString(c.get(Calendar.YEAR));
                String fecha;
                if (Integer.parseInt(mes) + 1 < 10) {
                    fecha = dia + "/0" + mes + "/" + annio;
                }
                else {
                    fecha = dia + "/" + mes + "/" + annio;
                }
                bd.connect();
                bd.introducirFecha(fecha, getApplicationContext());
                bd.alumnoAsiste(niu, fecha, getApplicationContext()); // Se marca la asistencia del alumno en la fecha de hoy
                bd.disconnect();
                finish();
                overridePendingTransition(R.anim.right_in,R.anim.right_out);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alumno_detectado, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
