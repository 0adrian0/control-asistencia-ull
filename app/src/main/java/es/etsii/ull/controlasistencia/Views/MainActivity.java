package es.etsii.ull.controlasistencia.Views;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;

import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.etsii.ull.controlasistencia.Data.BaseDatos;
import es.etsii.ull.controlasistencia.Business.PickFileWithOpenerActivity;
import es.etsii.ull.controlasistencia.R;
import es.etsii.ull.controlasistencia.Business.SyncRequestsActivity;


/**
 * Clase que gestiona la ventana principal donde se muestran las diferentes opciones disponibles
 */
public class MainActivity extends Activity {
    private BaseDatos bd;
    @SuppressWarnings("rawtypes")
    private ListView mListViewSamples;

    private static String PAQUETE = "es.etsii.ull.controlasistencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File folder = new File("/data/data/es.etsii.ull.controlasistencia/data");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }

        File file = new File("/data/data/es.etsii.ull.controlasistencia/data/dbDownloaded.csv");
        if (!file.exists()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("La aplicación necesita una base de datos de alumnos para funcionar. ¿Desea descargarla ahora?")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            elegirArchivo();
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            overridePendingTransition(R.anim.right_in, R.anim.right_out);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }

        try {
            this.bd = new BaseDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void iniciarCamara(View vi) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        alert.setTitle("Atención");
        alert.setMessage("Coloque el movil en horizontal para reconocer marcadores.");

        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent i = new Intent(MainActivity.this, CameraDetection.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        alert.show();
    }

    public void elegirArchivo() {
        Intent i = new Intent(getBaseContext(), PickFileWithOpenerActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    public void elegirArchivo(View vi) {
        Intent i = new Intent(getBaseContext(), PickFileWithOpenerActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    public void seleccionarAsignatura(View vi) {
        final SharedPreferences prefs = this.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        final String asignaturaActual = PAQUETE + ".asignaturaActual";
        String asignaturasAlmacenadas = PAQUETE + ".asignaturasAlmacenadas";

        final List<String> arrayAsginaturas; // Array con los nombres de las asignaturas almacenadas en el dispositivo
        arrayAsginaturas = new Gson().fromJson(prefs.getString(asignaturasAlmacenadas, ""), ArrayList.class);

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                MainActivity.this);
        builderSingle.setTitle("Seleccione asignatura:");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.select_dialog_singlechoice);
        for (int i = 0; i < arrayAsginaturas.size(); i++) {
            arrayAdapter.add(arrayAsginaturas.get(i));
        }
        builderSingle.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);

                        prefs.edit().putString(asignaturaActual, arrayAsginaturas.get(which)).apply();
                        TextView t = (TextView) findViewById(R.id.TVAsigActual);
                        t.setText(" " + strName);
                    }
                });
        builderSingle.show();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }

            File folder = new File("/data/data/es.etsii.ull.controlasistencia/data");
            deleteDir(folder);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public void eliminarAsignatura(View vi) {
        final SharedPreferences prefs = this.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        final String asignaturaActual = PAQUETE + ".asignaturaActual";
        final String asignaturasAlmacenadas = PAQUETE + ".asignaturasAlmacenadas";

        final List<String> arrayAsignaturas;
        arrayAsignaturas = new Gson().fromJson(prefs.getString(asignaturasAlmacenadas, ""), ArrayList.class);

        if (arrayAsignaturas.size() <= 1) { // Si sólo queda una asignatura, al borrarla se cierra la aplicación
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

            alert.setTitle("Atención");
            alert.setMessage("La aplicación necesita al menos una asignatura para funcionar. ¿Desea continuar?");
            final Context ctx = getApplicationContext();
            alert.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                    alert.setTitle("Atención");
                    alert.setMessage("La asignatura ha sido eliminada. Debe abandonar la aplicación.");
                    final Context ctx = getApplicationContext();
                    alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            deleteCache(ctx);
                            final SharedPreferences prefs = getSharedPreferences(
                                    PAQUETE, Context.MODE_PRIVATE);
                            prefs.edit().clear().commit();
                            finish(); // terminamos el intent actual
                        }
                    });
                    alert.show();
                }
            });

            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                }
            });
            alert.show();
        } else { // Si en la app hay 2 o más asignaturas...
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                    MainActivity.this);
            // Se muestra lista con las asignaturas y la seleccionada se borra
            builderSingle.setTitle("Seleccione asignatura:");
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    MainActivity.this,
                    android.R.layout.select_dialog_singlechoice);
            for (int i = 0; i < arrayAsignaturas.size(); i++) {
                arrayAdapter.add(arrayAsignaturas.get(i));
            }
            builderSingle.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                            alert.setTitle("Información");
                            alert.setMessage("Se ha eliminado la asignatura " + arrayAdapter.getItem(which));
                            final Context ctx = getApplicationContext();
                            alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            });

                            alert.show();
                            prefs.edit().putString(asignaturaActual, arrayAdapter.getItem(which)).apply();

                            bd.connect();
                            bd.borrarAsignatura(getApplicationContext());
                            bd.disconnect();
                            int posAsig = which;
                            if (posAsig - 1 >= 0)
                                posAsig--;
                            else
                                posAsig++;

                            prefs.edit().putString(asignaturaActual, arrayAsignaturas.get(posAsig)).apply();
                            TextView t = (TextView) findViewById(R.id.TVAsigActual);
                            t.setText(" " + arrayAsignaturas.get(posAsig));


                            final List<String> arrayAsignaturas;
                            arrayAsignaturas = new Gson().fromJson(prefs.getString(asignaturasAlmacenadas, ""), ArrayList.class);
                            arrayAsignaturas.remove(which);

                            prefs.edit().putString(asignaturasAlmacenadas, new Gson().toJson(arrayAsignaturas)).apply();

                        }
                    });
            builderSingle.show();
        }
    }

    /**
     * Método que carga a Google Drive un fichero CSV con la información de los alumnos y sus faltas de asistencia
     * de la asignatura actual.
     * @param vi
     */
    public void subirADrive(View vi) {
        bd.connect();
        bd.exportCsv(this);
        bd.disconnect();

        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        alert.setTitle("Nombre del fichero");
        alert.setMessage("Por favor, introduzca un nombre para el fichero generado.");

        // Set an EditText view to get user input
        final EditText input = new EditText(MainActivity.this);
        alert.setView(input);

        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                Editable value = input.getText();
                Intent i = new Intent(getBaseContext(), SyncRequestsActivity.class);
                i.putExtra("nombreFichero", value.toString());
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                dialog.dismiss();
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    // Cuando presione la tecla atrás le pregunto si quiere salir
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }

        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences prefs = this.getSharedPreferences(
                PAQUETE, Context.MODE_PRIVATE);
        final String asignaturaActual = PAQUETE + ".asignaturaActual";

        String nombreAsignatura = prefs.getString(asignaturaActual, "");

        TextView t = (TextView) findViewById(R.id.TVAsigActual);
        t.setText(" " + nombreAsignatura);
    }
}

